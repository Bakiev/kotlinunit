### Kotlin Unit example project

The project demonstrates `ClassCastException` "kotlin.Unit cannot be cast to java.lang.String" issue after bumping to Kotlin 1.5.+ (https://youtrack.jetbrains.com/issue/KT-47527)

### To reproduce:

- Run `./gradlew run`

### Notes:
- Can't be reproduced with Kotlin 1.4.32 and below
- Can be reproduced only if the function body is an expression
