fun <T : Any> getBad(value: T?): T? = value?.let { return "bad" as T}

fun <T : Any> getGood(value: T?): T? {
    return value?.let { return "good" as T}
}

fun main() {
    val value: String? = null
    val good = getGood(value)
    println("Good: $good")
    val bad = getBad(value)
    println("Bad: $bad")
}
